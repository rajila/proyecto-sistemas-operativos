/**
 * PROYECTO DE SISTEMAS OPERATIVOS RAPIDITO.NET
 * La empresa de acceso a internet PUNTOWWW tiene un nuevo servicio llamado Rapiditonet que ajusta la velocidad
 * de navegación segun el plan que adquiere un cliente. 
 * La idea es que el cliente que tenga contratado un plan mas caro, su conexion sea mas rapida que los demas y el
 * tiempo de respuesta de cargar la pagina sea mucho mejor. Existen tres tipos de planes:
 *  A: Plan conectado
 *  B: Plan de redes sociales
 *  C: Plan VIP
 * Paginas de Ayuda:
 *  http://totaki.com/poesiabinaria/2011/02/creando-un-servidor-que-acepte-multiples-clientes-simultaneos-en-c/
 *  http://www.davidlopez.es/chat-multihilo-con-sockets-en-python-y-en-c/
 * Autore:
 *  Ronald Daniel Ajila.(radc)
 *  ESPOL
 */

#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

#define BUFLEN 512
#define NPACK 200 // Numero de clientes conectados
#define PORT 5555
#define MAXCLIENTS 1000
#define TIME_ESPERA 10

// Estructura del cliente
typedef struct cliente
{
	int _s_socket; // Id del socket
	char _plan; // Tipo de plan del cliente
	char *_url; // Url que desea cargar
	time_t _tiempoLlegada;
}cliente_t;

// Estructura para el buffer
typedef struct buffer
{
	cliente_t *_clientes; // Cola de clientes.
	int _dimC; // dimension actual de la cola
	pthread_mutex_t _mutextBuffer; // mutex, ya que los hilos del mismo buffer actualizan la cola sincronicamente.
	pthread_t _hilos[]; // Nùmero de hilos del buffer
}buffer_t;

buffer_t _bA;
buffer_t _bB;
buffer_t _bC;

pthread_mutex_t _mutexWrite;

int N;
int clientes = 0;

// Tiempo
time_t _tiempo;
struct tm *_tmpTiempo;
struct tm _fechaTiempo;

void *leeBuffer( void *parametro );
void* gestionarPaquete( void* parametro );
void agregarPlanBuffer( buffer_t *buffer, cliente_t *cliente );
void eliminaPlanBuffer (buffer_t *X, int indice);

int main( int argc, char *argv[] )
{
	int i = 0, id;
	char tmp = 'A';
	char planA = 'A';
	char planB = 'B';
	char planC = 'C';
	int status;
	
	pthread_t hilos[MAXCLIENTS];
	
	// Verificamos que la entrada del parametro sea el correcto. ni más ni menos parametros
	if(argc != 2 )
	{
		fprintf( stderr, "Ejecutar: ./servidor <value_N>\n" );
		return -1;
	}
	
	// El valor de N mayor/igual a 4
	if( atoi(argv[1]) < 4 )
	{
		fprintf( stderr, "%d debe ser >= 4\n", atoi(argv[1]) );
		return -1;
	}
	
	// Nùmero de hilos para cada buffer
	N = atoi(argv[1]);
	int tamA = (int)N/4;
	int tamB = (int)N/2;
	int tamC = N;
	int totalDim = tamC + tamB + tamA;
	
	// Dimensión de la lista de clientes.
	_bA._dimC = 0;
	_bB._dimC = 0;
	_bC._dimC = 0;
	
	// Se ejecutan todos los hilos del buffer asincronicamente.
	for( i=0; i<totalDim; i++ )
	{	
		if( i <= tamA-1 && tmp == 'A' )
		{
			pthread_create( &_bA._hilos[i], NULL, leeBuffer, (void *)&planA );
			totalDim--;
			if( i == tamA-1 )
			{
				i=-1;
				tmp = 'B';
				continue;
			}
		}
		if( i <= tamB-1 && tmp == 'B' )
		{
			pthread_create( &_bB._hilos[i], NULL, leeBuffer, (void *)&planB );
			totalDim--;
			if( i == tamB-1 )
			{
				i=-1;
				tmp = 'C';
				continue;
			}
		}
		if( i <= tamC-1 && tmp == 'C' ) pthread_create( &_bC._hilos[i], NULL, leeBuffer, (void *)&planC );
	}
	
	//pthread_mutex_init( &_mutexWrite, NULL ); // Inicializa el MUTEX
	
	pthread_mutex_init( &_bA._mutextBuffer, NULL ); // Inicializa el MUTEX
	pthread_mutex_init( &_bB._mutextBuffer, NULL ); // Inicializa el MUTEX
	pthread_mutex_init( &_bC._mutextBuffer, NULL ); // Inicializa el MUTEX
	
	struct sockaddr_in si_me, si_other;
	int s, slen=sizeof(si_other), new_sd;
	char buf[BUFLEN];

	if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP))==-1) perror("sock err"); // Si no hay conexión.
	memset((char *) &si_me, 0, sizeof(si_me));
	
	// Bind local address to allow the client to connect
	si_me.sin_family = AF_INET;
	si_me.sin_addr.s_addr = htonl(INADDR_ANY);
	si_me.sin_port = htons(PORT);
	
	if (bind(s, &si_me, sizeof(si_me))==-1) perror("bind");
	
	for (;;) 
	{
		if( recvfrom(s, buf, BUFLEN, 0, &si_other, &slen) == -1 ) 
		{
			printf("Error aceptando peticiones\n");
			exit(0);
		}	
		else{
			id = clientes;
			clientes++;
			
			printf("Received packet from: %s\n", buf);
			i=0;
			int mod = 0;
			int j=0;
			char tmp_plan[100];
			char tmp_id[100];
			char tmp_url[100];
			cliente_t cliente;
			for(i=0; i<strlen(buf); i++)
			{
				if(buf[i] == ',') mod=1;
				if(mod==0) tmp_plan[i] = buf[i];
				if(mod==1 && buf[i] != ',') tmp_url[j] = buf[i];
				if(mod==1 && buf[i] != ',') j++;
			}
			for(i=0; i<strlen(tmp_plan)-1; i++) tmp_id[i] = tmp_plan[i+1];
			
			cliente._s_socket = atoi(tmp_id);
			cliente._plan = tmp_plan[0];
			cliente._url = tmp_url;
			cliente._tiempoLlegada =  time(NULL);
			
			printf("plan: %c ::: url: %s ::: id: %d\n\n", cliente._plan, tmp_url, cliente._s_socket);
			
			if ( (status = pthread_create(&hilos[id],NULL,gestionarPaquete,(void *)&cliente)) ) //new_sd
			{
				printf("Error al crear el hilo\n");
				exit(0);
			}
		}
	}
	
	close(s);
	return 0;
}

/**
 * Cuando se envia un paquete al servidor este lo guarda en el buffer de acuerdo al tipo
 * de plan del cliente.
 */
void* gestionarPaquete( void* parametro )
{
	cliente_t cliente = *(cliente_t *)parametro;
	int id;
	id = cliente._s_socket;
	char buffer[BUFLEN];
	
	if( cliente._plan == 'A' )
	{
		pthread_mutex_lock( &_bA._mutextBuffer);
			agregarPlanBuffer( &_bA, &cliente );
		pthread_mutex_unlock( &_bA._mutextBuffer );
	}
	
	if( cliente._plan == 'B' )
	{
		pthread_mutex_lock( &_bB._mutextBuffer );
			agregarPlanBuffer( &_bB, &cliente );
		pthread_mutex_unlock( &_bB._mutextBuffer );
	}
	
	if( cliente._plan == 'C' )
	{
		pthread_mutex_lock( &_bC._mutextBuffer );
			agregarPlanBuffer( &_bC, &cliente );
		pthread_mutex_unlock( &_bC._mutextBuffer );
	}
	
	//send(id,buffer,BUFLEN,0);
	//close(id);
	
	fflush(stdout);
	pthread_exit( NULL );
}

/**
 * Lee el buffer, para corrobar que hay paquetes y proceder a atenderlos, 
 * tambien se verifica la prioridad que tiene el paquete. 
 */
void *leeBuffer( void *parametro )
{
	char plan = *(int* )parametro;
	printf("inicio::%c\n",plan);
	char buffer[BUFLEN];
	int tiempo_espera = 0;
	
	// Tiempo
	time_t _tiempoPlan;
	struct tm *_tmpTiempoPlan;
	struct tm _fechaTiempoPlan;
	
	while(1)
	{
		_tiempo = time(NULL);
		_tmpTiempo = localtime( &_tiempo );
		memcpy(&_fechaTiempo, _tmpTiempo, sizeof _fechaTiempo);
		sleep(3);
		if( plan == 'A' )
		{
			pthread_mutex_lock( &_bA._mutextBuffer );
				if( _bA._dimC != 0 )
				{
					_tiempoPlan = _bA._clientes[0]._tiempoLlegada;
					_tmpTiempoPlan = localtime( &_tiempoPlan );
					memcpy(&_fechaTiempoPlan, _tmpTiempoPlan, sizeof _fechaTiempoPlan);
					if( abs( ((_fechaTiempo.tm_hour*60*60)+(_fechaTiempo.tm_min*60)+_fechaTiempo.tm_sec)- ((_fechaTiempoPlan.tm_hour*60*60)+(_fechaTiempoPlan.tm_min*60)+_fechaTiempoPlan.tm_sec) ) > TIME_ESPERA )
					{
						sprintf(buffer,"PID:%d", _bA._clientes[0]._s_socket);
						printf( "A ::: PID: %s ::: msn: Demasido tiempo de espera del paquete: %d seg\n", buffer, abs( ((_fechaTiempo.tm_hour*60*60)+(_fechaTiempo.tm_min*60)+_fechaTiempo.tm_sec)- ((_fechaTiempoPlan.tm_hour*60*60)+(_fechaTiempoPlan.tm_min*60)+_fechaTiempoPlan.tm_sec) ) );
						eliminaPlanBuffer (&_bA, 0);
					}else
						if(  _bA._dimC != 0 && _bB._dimC == 0 && _bC._dimC == 0  )
						{
							sprintf(buffer,"PID:%d", _bA._clientes[0]._s_socket);
							printf("A ::: PID: %s\n", buffer);
							eliminaPlanBuffer (&_bA, 0);
						}
				}
			pthread_mutex_unlock( &_bA._mutextBuffer );
// 			printf("A: %c :::: dim: %d\n", plan, _bA._dimC);
		}
		if( plan == 'B' )
		{
			pthread_mutex_lock( &_bB._mutextBuffer );
				if( _bB._dimC != 0 )
				{
					_tiempoPlan = _bB._clientes[0]._tiempoLlegada;
					_tmpTiempoPlan = localtime( &_tiempoPlan );
					memcpy(&_fechaTiempoPlan, _tmpTiempoPlan, sizeof _fechaTiempoPlan);
					
					if( abs( ((_fechaTiempo.tm_hour*60*60)+(_fechaTiempo.tm_min*60)+_fechaTiempo.tm_sec)- ((_fechaTiempoPlan.tm_hour*60*60)+(_fechaTiempoPlan.tm_min*60)+_fechaTiempoPlan.tm_sec) ) > TIME_ESPERA )
					{
						sprintf(buffer,"PID:%d", _bB._clientes[0]._s_socket);
						printf( "B ::: PID: %s ::: msn: Demasido tiempo de espera del paquete: %d seg\n", buffer, abs( ((_fechaTiempo.tm_hour*60*60)+(_fechaTiempo.tm_min*60)+_fechaTiempo.tm_sec)- ((_fechaTiempoPlan.tm_hour*60*60)+(_fechaTiempoPlan.tm_min*60)+_fechaTiempoPlan.tm_sec) ) );
						eliminaPlanBuffer (&_bB, 0);
					}else
						if(  _bB._dimC != 0 &&  _bC._dimC == 0 )
						{
							sprintf(buffer,"PID:%d", _bB._clientes[0]._s_socket);
							printf("B ::: PID: %s\n", buffer);
							eliminaPlanBuffer (&_bB, 0);
						}
				}
			pthread_mutex_unlock( &_bB._mutextBuffer );
// 			printf("B: %c\n", plan);
		}
		if( plan == 'C' )
		{
			pthread_mutex_lock( &_bC._mutextBuffer );
				if(  _bC._dimC != 0 )
				{
					sprintf(buffer,"PID:%d", _bC._clientes[0]._s_socket);
// 					send(_bC._clientes[0]._s_socket,buffer,BUFLEN,0);
					printf("C ::: PID: %s\n", buffer);
// 					close(_bC._clientes[0]._s_socket);
					eliminaPlanBuffer (&_bC, 0);
				}
			pthread_mutex_unlock( &_bC._mutextBuffer );
// 			printf("C: %c\n", plan);
		}
	}
	pthread_exit( NULL );
}

/**
 * Agrega un cliente a la lista de buffer, para tener referencia a los clientes con planes que llegan.
 * http://www.ibserveis.com/fpro/Arrays_CSHARP_visto_desde_C.pdf 
 */
void agregarPlanBuffer( buffer_t *buffer, cliente_t *cliente )
{
	buffer->_clientes = (cliente_t *)realloc( buffer->_clientes, sizeof(cliente_t)*(buffer->_dimC+1));
	buffer->_clientes[buffer->_dimC++] = *cliente;
}

/**
 * Elimina un cliente a la lista de buffer, para tener referencia a los clientes con planes que llegan.
 * http://www.ibserveis.com/fpro/Arrays_CSHARP_visto_desde_C.pdf 
 */
void eliminaPlanBuffer (buffer_t *X, int indice)
{
	int i;
	for (i=indice ; i<X->_dimC-1 ; i++) X->_clientes[i] = X->_clientes[i+1];
	X->_clientes = (cliente_t*) realloc (X->_clientes,sizeof(cliente_t)*(--X->_dimC));
}